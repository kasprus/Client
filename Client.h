/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Client.h
 * Author: kasprus
 *
 * Created on 13 maja 2017, 19:29
 */

#ifndef CLIENT_H
#define CLIENT_H

#include <iostream>
#include <boost/asio.hpp>
#include <boost/array.hpp>
//using boost::asio::ip::tcp;

class Client{
private:
    boost::asio::io_service io_service;
    boost::asio::ip::tcp::endpoint endpoint;
    boost::asio::ip::tcp::socket socket;
public:
    bool send(std::string a){
        boost::system::error_code error;
        //socket.write_some(boost::asio::buffer(buf, message.size()), error);
        boost::array<char, 128> buf;
        buf.fill(0);
        std::copy(a.begin(), a.end(), buf.begin());
        socket.write_some(boost::asio::buffer(buf, buf.size()), error);
        
    }
    Client(std::string ip, int port): endpoint(boost::asio::ip::address::from_string(ip), port), socket(io_service){
        socket.connect(endpoint);
    }
    void reconnect(){
        socket.close();
        socket.connect(endpoint);
    }
    ~Client(){
        socket.close();
    }
};

#endif /* CLIENT_H */

